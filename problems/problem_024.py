# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if values == []:
        return None
    else:
        total = sum(values)/len(values)
    return total

print(calculate_average([23,24,24,15]))
print(calculate_average([1,2,3,4]))
print(calculate_average([23,24,24,15]))
print(calculate_average([]))
